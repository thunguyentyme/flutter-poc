enum Flavor {
  SIT,
  UAT,
}

class F {
  static Flavor appFlavor;

  static String get title {
    switch (appFlavor) {
      case Flavor.SIT:
        return 'TB SIT';
      case Flavor.UAT:
        return 'TB UAT';
      default:
        return 'title';
    }
  }

}
