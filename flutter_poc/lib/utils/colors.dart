import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AppColor {
  static const Color main_background = Color(0xFF231F20);
  static const Color yellow = Color(0xFFffcc00);
  static const Color white = Color(0xFFFFFFFF);
  static const Color black = Color(0xFF000000);
  static const Color grey_30 = Color(0xFFB2B2B2);
  static const Color transparent = Colors.transparent;
  static const Color gradient_top = Color(0xFF1D1D1D);
  static const Color gradient_bottom = Color(0xFFACACAC);
  static const Color yellow_disabled = Color(0xFFFDC600);
}