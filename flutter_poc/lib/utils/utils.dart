import 'dart:convert';
import 'dart:math';
import'dart:io' show Platform;

import 'package:asn1lib/asn1lib.dart';
import 'package:flutter_poc/config/AppConfig.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_instance/src/extension_instance.dart';
import "package:pointycastle/export.dart";
import 'package:device_info_plus/device_info_plus.dart';

class Utils {

  static String _deviceInfoHeader = "";

  static Future<void> generateDeviceInfoHeader() async {
    var json = {
      "device_id": getDeviceId(),
      "device_name": getDeviceName(),
      "device_os": "${getOSName()} ${await getDeviceOS()}",
      "firebase_id": "",
      "geolocation": "",
      "mac": await getMacAddress(),
      "sim_info": [],
      "version": getAppVersion()
    };

    _deviceInfoHeader = base64Encode(utf8.encode(jsonEncode(json)));
  }

  static String getDeviceInfoHeader() {
    return _deviceInfoHeader;
  }

  static String getDeviceName() {
    return "Flutter";
  }

  static String getDeviceId() {
    return "emulator";
  }

  static String getSerialNumber() {
    return "emulator";
  }

  static String getOSName() {
    return Platform.operatingSystem;
  }

  static String getAppVersion() {
    return "V1.24.10-PRE210922D";
  }

  static Future<String> getMacAddress() async {
    DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
    if (Platform.isAndroid) {
      AndroidDeviceInfo info = await deviceInfo.androidInfo;
      return info.androidId;
    }

    if (Platform.isIOS) {
      IosDeviceInfo info = await deviceInfo.iosInfo;
      return info.identifierForVendor;
    }

    return "";
  }

  static Future<String> getDeviceOS() async {
    DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
    if (Platform.isAndroid) {
      AndroidDeviceInfo info = await deviceInfo.androidInfo;
      return info.version.release;
    }

    if (Platform.isIOS) {
      IosDeviceInfo info = await deviceInfo.iosInfo;
      return info.systemVersion;
    }

    return "";
  }

  static String encryptLoginPin(String said, String pin) {
    String profilePinBlod1 = encryptPinBlod1(pin);
    return encryptPinBlod2(
        said, profilePinBlod1);
  }

  static String encryptPinBlod1(String pin) {
    var config = Get.find<AppConfig>();
    return encrypt(config.getProfilePinEncryptionKey(),
        pin + config.getProfilePinSalt(), false);
  }

  static String encryptPinBlod2(String said, String pinBlod1) {
    var config = Get.find<AppConfig>();
    var time = DateTime.now().millisecondsSinceEpoch;
    var nonce = createNonce();
    String encryptedPassword = encrypt(config.getProfilePinTransportKey(),
        "$nonce:$time:$pinBlod1", true);

    String credential = said + ":" + encryptedPassword;
    return base64Encode(utf8.encode(credential));
  }

  static String encrypt(String publicKey, String plainText, bool isPadding) {
    String result = "";
    try {
      var pubKey = parsePublicKeyFromPem(publicKey);

      AsymmetricBlockCipher cipher;
      if(isPadding) cipher = PKCS1Encoding(RSAEngine());
      else cipher = RSAEngine();
      cipher..init(true, PublicKeyParameter<RSAPublicKey>(pubKey));

      return base64Encode(cipher.process(utf8.encode(plainText)));
    } catch (e) {
      print("Error while encrypt: $plainText");
      print(e);
    }
    return result;
  }

  /// Decode Public key from PEM Format
  ///
  /// Given a base64 encoded PEM [String] with correct headers and footers, return a
  /// [RSAPublicKey]
  ///
  /// *PKCS1*
  /// RSAPublicKey ::= SEQUENCE {
  ///    modulus           INTEGER,  -- n
  ///    publicExponent    INTEGER   -- e
  /// }
  ///
  /// *PKCS8*
  /// PublicKeyInfo ::= SEQUENCE {
  ///   algorithm       AlgorithmIdentifier,
  ///   PublicKey       BIT STRING
  /// }
  ///
  /// AlgorithmIdentifier ::= SEQUENCE {
  ///   algorithm       OBJECT IDENTIFIER,
  ///   parameters      ANY DEFINED BY algorithm OPTIONAL
  /// }
  static RSAPublicKey parsePublicKeyFromPem(pemString) {
    List<int> publicKeyDER = base64Decode(pemString);
    var asn1Parser = new ASN1Parser(publicKeyDER);
    var topLevelSeq = asn1Parser.nextObject() as ASN1Sequence;

    var modulus, exponent;
    // Depending on the first element type, we either have PKCS1 or 2
    if (topLevelSeq.elements[0].runtimeType == ASN1Integer) {
      modulus = topLevelSeq.elements[0] as ASN1Integer;
      exponent = topLevelSeq.elements[1] as ASN1Integer;
    } else {
      var publicKeyBitString = topLevelSeq.elements[1];

      var publicKeyAsn = new ASN1Parser(publicKeyBitString.contentBytes());
      ASN1Sequence publicKeySeq = publicKeyAsn.nextObject();
      modulus = publicKeySeq.elements[0] as ASN1Integer;
      exponent = publicKeySeq.elements[1] as ASN1Integer;
    }

    return RSAPublicKey(modulus.valueAsBigInteger, exponent.valueAsBigInteger);
  }

  static int createNonce() {
    return new Random.secure().nextInt((1 << 32) - 1);
  }
}
