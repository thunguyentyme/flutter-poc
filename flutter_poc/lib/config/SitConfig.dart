import 'package:flutter_poc/config/AppConfig.dart';

class SitConfig implements AppConfig {
  @override
  String getBaseUrl() {
    return "https://chapigw-sa1.testtymebank.co.za";
  }

  @override
  String getClientIdNonDevice() {
    return "0H_05I591cUwMII6VPAHSZf7_s8a";
  }

  @override
  String getClientSecretNonDevice() {
    return "q3ZX2G7FoQ58i52vMRarBCwGorsa";
  }

  @override
  String getClientIdLinkedDevice() {
    return "HGObULnbvGD68vibL_qqO5fLSW4a";
  }

  @override
  String getClientSecretLinkedDevice() {
    return "VSqrl158hzIoyEoOQ9EXOK2jeK0a";
  }

  @override
  String getProfilePinEncryptionKey() {
    return "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAkz" +
        "Y8GHwUCIvbtOVeK0rnBWMwUSxVhxvrO65wN5Wdg6hsDJomIGj4KAEyXV/qrNQPUdfSlli/dkeb633JjTfTkben7" +
        "DIjDT4uyo+NS2fKbFh3Vibr/oKJ/lhWZ9l84wXy5Hph7uKmwraQkkuI9HoWhzcFOIyu1gmRN4MAd438lCmWA2yn" +
        "xsN2ErGe/WgqgsIcErilUKgaqcCCoSFMuJfR9+IqcPFWRyizbYiOIylUPwxDB4UTvToOu37G2OBKbi1mczGaC+7" +
        "UdCkwNJW/+qOjOf1M1zbkaCnIiXgRokjnNkjGYSkett20rP+q0XU+7fWZUqSpbw3SHmEoWh+r4tkbWwIDAQAB";
  }

  @override
  String getProfilePinSalt() {
    return "CA68DE6163C1133B34C62D3BFE7F6";
  }

  @override
  getProfilePinTransportKey() {
    return "MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEAm6etezcNiV42jt8SJeVY" +
        "WBl2czkR8YHcEKl7eVI0JmwKmexB7J4he4oKKzHGpf7n1FGj4lFiEJRscgM71vV5" +
        "DFbnIMA+crPj+UY0J3sMUSIjSGOkOR9ck0SvoD+XnA3cRXIzw4SUw1hIgDvOEBp9" +
        "8B36FiQ5OhXZf6Q8oVAQhEPHSEAXQ/8rZCit3WhzifSKi7cWYeCmNqEknTr5kXqI" +
        "8X86u/Y+2ptJ/tQMsOmJ2xBvhcb8Jy4U/06eXk4wvoNMwpoWXVcZG8xGu9rq6x+d" +
        "a+L6+QFiui936Ojnf5CtD7+3IZfsIAI4H9BUC/zrAa9piVZaDeogX/a7lmASzOYB" +
        "Kw2i8ESq4BMpiFhRxEG4FemcWltNwRNzA+W4OevND3HeZ0ybhzUpRcIlQFbW0WWn" +
        "EIr1V16SaMg/abjh9AQBuQdJ+3795pkKo6m9UQ2vAfjfJn6XiJWP3xGWA4RzXppp" +
        "KMky6qph9U7D0fKVFLLbrwqD7yjavQzssiSYOKsxqEzOQclkSjlHXQVWpPo2uLRa" +
        "XZVV/R1t+5x7EYkZYIapwfatX450n6eVw7Clf25SDCb9oWYBuoeqkcuqeCA2eGTi" +
        "arkyiEsRB5eEBpixduC7wRzALtCiLIyfeDzkarb4hOFXhIEbao0BebC5zXi6ZRKy" +
        "V6CV241ZquhwfkvpyFQ+rX0CAwEAAQ==";
  }
}
