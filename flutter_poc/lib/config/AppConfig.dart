abstract class AppConfig {
  String getBaseUrl();
  String getClientIdNonDevice();
  String getClientSecretNonDevice();
  String getClientIdLinkedDevice();
  String getClientSecretLinkedDevice();
  String getProfilePinEncryptionKey();
  String getProfilePinSalt();
  String getProfilePinTransportKey();
}