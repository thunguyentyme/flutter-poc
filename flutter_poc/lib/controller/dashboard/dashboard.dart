import 'package:flutter_poc/data/cache/app_cache.dart';
import 'package:flutter_poc/data/local/prefs/constant.dart';
import 'package:flutter_poc/data/model/account.dart';
import 'package:flutter_poc/data/repository/account.dart';
import 'package:flutter_poc/data/repository/moretyme.dart';
import 'package:flutter_poc/route/app_pages.dart';
import 'package:get/get.dart';
import 'package:meta/meta.dart';
import 'package:rxdart/rxdart.dart';
import 'package:shared_preferences/shared_preferences.dart';

class DashboardController extends GetxController {
  final AccountRepository accountRepository;
  final MoreTymeRepository moreTymeRepository;
  final AppCache appCache;

  DashboardController(
      {@required this.accountRepository,
      @required this.moreTymeRepository,
      @required this.appCache})
      : assert(accountRepository != null),
        assert(moreTymeRepository != null),
        assert(appCache != null);

  var oTransactionAccountName = "----".obs;
  var oTransactionAccountNumber = "----".obs;
  var oTransactionAccountBalance = "----".obs;
  var oCountGoalSave = "----".obs;
  var oSumGoalSave = "----".obs;
  var oMTAts = "----".obs;
  var oMTLimit = "----".obs;

  @override
  void onInit() {
    super.onInit();
    _doGetAccount();
    _doGetMoreTymeAccount();
  }
  void logOut() {
    Get.toNamed(Routes.PRE_LOGIN);
  }

  Future<void> delinkDevice() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(Constant.saidKey, "");

    logOut();
  }


  void _doGetAccount() {
    Stream.fromFuture(accountRepository.getAccounts()).doOnData((accounts) {
      Account edaAccount;
      int countGoalSave = 0;
      double sumGoalSave = 0;


      for (Account account in accounts) {
        if (account.isActive()) {
          if (account.isTransactionAccount()) edaAccount = account;
          if (account.isGoalSaveAccount()) {
            countGoalSave++;
            sumGoalSave += (double.tryParse(account.balance) ?? 0)
                + (double.tryParse(account.accruedInterest) ?? 0);
          }
        }
      }

      oTransactionAccountName.value = edaAccount.accountName;
      oTransactionAccountNumber.value = edaAccount.accountNumber;
      oTransactionAccountBalance.value = edaAccount.balance;

      oCountGoalSave.value = countGoalSave.toString();
      oSumGoalSave.value = sumGoalSave.toStringAsFixed(2);
    }).doOnError((e, stackTrace) {
      print(e);
    }).listen(null);
  }

  void _doGetMoreTymeAccount() {
    Stream.fromFuture(moreTymeRepository.getAccount()).doOnData((account) {
      oMTAts.value = account.ats;
      oMTLimit.value = account.limit;
    }).doOnError((e, stackTrace) {
      print(e);
    }).listen(null);
  }
}
