import 'package:flutter/cupertino.dart';
import 'package:flutter_poc/data/cache/app_cache.dart';
import 'package:flutter_poc/data/repository/authentication.dart';
import 'package:flutter_poc/route/app_pages.dart';
import 'package:get/get.dart';
import 'package:meta/meta.dart';
import 'package:rxdart/rxdart.dart';

class PinController extends GetxController {
  final AuthenticationRepository repository;
  final AppCache appCache;

  final pinController = TextEditingController();

  PinController({@required this.repository, @required this.appCache})
      : assert(repository != null), assert(appCache != null);


  goToOtpPage() {
    appCache.pin = pinController.text;
    Get.toNamed(Routes.OTP);
  }

  @override
  void onClose() {
    pinController.dispose();
    super.onClose();
  }

  void onPinEntered() {
    Stream.fromFuture(repository.login(appCache.said, pinController.text))
        .doOnData((info) {
      _doGenerateToken(info.code);
    }).doOnError((e, stackTrace) {
      print(e);
    }).listen(null);
  }

  void _doGenerateToken(String code) {
    Stream.fromFuture(repository.generateToken(code)).doOnData((token) {
      appCache.authorizationToken = "${token.tokenType} ${token.accessToken}";
      _doGenerateOtpRequestToken();
    }).doOnError((e, stackTrace) {
      print(e);
    }).listen(null);
  }

  void _doGenerateOtpRequestToken() {
    Stream.fromFuture(repository.generateOtpRequestToken()).doOnData((token) {
      appCache.otpRequestToken = token;
      goToOtpPage();
    }).doOnError((e, stackTrace) {
      print(e);
    }).listen(null);
  }
}
