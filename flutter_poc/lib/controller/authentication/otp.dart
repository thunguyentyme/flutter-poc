import 'package:flutter/cupertino.dart';
import 'package:flutter_poc/data/cache/app_cache.dart';
import 'package:flutter_poc/data/repository/authentication.dart';
import 'package:flutter_poc/route/app_pages.dart';
import 'package:flutter_poc/data/local/prefs/constant.dart';
import 'package:get/get.dart';
import 'package:meta/meta.dart';
import 'package:rxdart/rxdart.dart';
import 'package:shared_preferences/shared_preferences.dart';

class OtpController extends GetxController {
  final AuthenticationRepository repository;
  final AppCache appCache;

  final otpController = TextEditingController();
  var oDeliveryTo = "".obs;

  OtpController({@required this.repository, @required this.appCache})
      : assert(repository != null), assert(appCache != null);


  goToPreLoginPage() {
    Get.toNamed(Routes.PRE_LOGIN);
  }

  @override
  void onInit() {
    super.onInit();
    _doRequestOtp();
  }

  @override
  void onClose() {
    otpController.dispose();
    super.onClose();
  }

  void _doRequestOtp() {
    Stream.fromFuture(repository.requestOtp(appCache.otpRequestToken)).doOnData((otpInfo) {
      oDeliveryTo.value = otpInfo.deliveryTo;
    }).doOnError((e, stackTrace) {
      print(e);
    }).listen(null);
  }

  void onOtpEntered() {
    Stream.fromFuture(repository.verifyOtp(appCache.otpRequestToken, otpController.text)).doOnData((stepUpToken) {
      _doAddDevice(stepUpToken);
    }).doOnError((e, stackTrace) {
      print(e);
    }).listen(null);
  }

  void _doAddDevice(String stepUpToken) {
    Stream.fromFuture(repository.addDevice(stepUpToken)).doOnData((success) async {
      await _saveUserInfo();
      goToPreLoginPage();
    }).doOnError((e, stackTrace) {
      print(e);
    }).listen(null);
  }

  _saveUserInfo() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(Constant.saidKey, appCache.said);
  }
}
