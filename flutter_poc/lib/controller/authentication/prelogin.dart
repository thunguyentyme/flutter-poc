import 'package:flutter/cupertino.dart';
import 'package:flutter_poc/data/cache/app_cache.dart';
import 'package:flutter_poc/data/local/prefs/constant.dart';
import 'package:flutter_poc/data/repository/authentication.dart';
import 'package:flutter_poc/route/app_pages.dart';
import 'package:get/get.dart';
import 'package:meta/meta.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PreLoginController extends GetxController {
  final AuthenticationRepository repository;
  final AppCache appCache;

  PreLoginController({@required this.repository, @required this.appCache})
      : assert(repository != null), assert(appCache != null);

  goToLoginPage() {
    Get.toNamed(Routes.LOGIN_PIN);
  }

  goToSaidPage() {
    Get.toNamed(Routes.SAID);
  }

  @override
  void onInit() {
    super.onInit();
    _retrieveUserInfo();
  }

  Future<void> _retrieveUserInfo() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    appCache.said = prefs.getString(Constant.saidKey) ?? "";
    appCache.pin = prefs.getString(Constant.pinKey) ??  "";
  }

  void onClickLogin() {
    if (appCache.said.isBlank) goToSaidPage();
    else goToLoginPage();
  }
}
