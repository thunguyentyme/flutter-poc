import 'package:flutter/cupertino.dart';
import 'package:flutter_poc/data/cache/app_cache.dart';
import 'package:flutter_poc/data/repository/authentication.dart';
import 'package:flutter_poc/route/app_pages.dart';
import 'package:get/get.dart';
import 'package:meta/meta.dart';

class SaidController extends GetxController {
  final AuthenticationRepository repository;
  final AppCache appCache;

  final saidController = TextEditingController();

  SaidController({@required this.repository, @required this.appCache})
      : assert(repository != null), assert(appCache != null) {
    print("hi");
  }

  goToPinPage() {
    appCache.said = saidController.text;
    Get.toNamed(Routes.PIN);
  }

  @override
  void onClose() {
    saidController.dispose();
    super.onClose();
  }

  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }
}
