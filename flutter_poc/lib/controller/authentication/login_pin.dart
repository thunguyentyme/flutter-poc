import 'package:flutter_poc/controller/authentication/pin.dart';
import 'package:flutter_poc/data/cache/app_cache.dart';
import 'package:flutter_poc/data/repository/authentication.dart';
import 'package:flutter_poc/route/app_pages.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_navigation/src/extension_navigation.dart';
import 'package:rxdart/rxdart.dart';

class LoginPinController extends PinController {
  LoginPinController({AuthenticationRepository repository, AppCache appCache})
      : super(repository: repository, appCache: appCache);

  goToDashboardPage() {
    appCache.pin = pinController.text;
    Get.toNamed(Routes.DASH_BOARD);
  }

  @override
  void onPinEntered() {
    Stream.fromFuture(repository.loginOnLinkedDevice(appCache.said, pinController.text))
        .doOnData((info) {
      _doGenerateToken(info.code);
    }).doOnError((e, stackTrace) {
      print(e);
    }).listen(null);
  }

  void _doGenerateToken(String code) {
    Stream.fromFuture(repository.generateTokenOnLinkedDevice(code)).doOnData((token) {
      appCache.authorizationToken = "${token.tokenType} ${token.accessToken}";
      goToDashboardPage();
    }).doOnError((e, stackTrace) {
      print(e);
    }).listen(null);
  }
}
