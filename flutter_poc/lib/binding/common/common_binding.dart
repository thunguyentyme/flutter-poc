import 'package:dio/dio.dart';
import 'package:flutter_poc/config/AppConfig.dart';
import 'package:flutter_poc/config/SitConfig.dart';
import 'package:flutter_poc/config/UatConfig.dart';
import 'package:flutter_poc/data/cache/app_cache.dart';
import 'package:flutter_poc/data/provider/api_client.dart';
import 'package:flutter_poc/data/repository/account.dart';
import 'package:flutter_poc/data/repository/authentication.dart';
import 'package:flutter_poc/data/repository/moretyme.dart';
import 'package:get/get.dart';

import '../../flavors.dart';

class CommonBindings implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<AppConfig>(() {
      switch (F.appFlavor) {
        case Flavor.SIT:
          return SitConfig();
        case Flavor.UAT:
          return UatConfig();
        default:
          throw UnimplementedError();
      }
    });

    Get.lazyPut<Iterable<Interceptor>>(() {
      return [LogInterceptor(requestBody: true, responseBody: true)];
    }, tag: "defaultInterceptors");

    Get.lazyPut<Dio>(() {
      var config = Get.find<AppConfig>();
      var options = BaseOptions(
          baseUrl: config.getBaseUrl(),
          followRedirects: false,
          connectTimeout: 5000,
          receiveTimeout: 5000);

      var dio = new Dio(options);
      dio.interceptors.addAll(Get.find(tag: "defaultInterceptors"));
      return dio;
    }, tag: "defaultHttpClient");

    Get.lazyPut<AppCache>(() {
      return AppCache();
    });

    Get.lazyPut<HttpApiClient>(() {
      return HttpApiClient(
          httpClient: Get.find(tag: "defaultHttpClient"),
          appCache: Get.find<AppCache>());
    });

    Get.lazyPut<AuthenticationRepository>(() {
      return AuthenticationRepository(
          apiClient: Get.find<HttpApiClient>(),
          appConfig: Get.find<AppConfig>());
    });

    Get.lazyPut<MoreTymeRepository>(() {
      return MoreTymeRepository(apiClient: Get.find<HttpApiClient>());
    });

    Get.lazyPut<AccountRepository>(() {
      return AccountRepository(apiClient: Get.find<HttpApiClient>());
    });
  }
}
