import 'package:flutter_poc/controller/dashboard/dashboard.dart';
import 'package:flutter_poc/data/cache/app_cache.dart';
import 'package:flutter_poc/data/repository/account.dart';
import 'package:flutter_poc/data/repository/moretyme.dart';
import 'package:get/get.dart';

class DashboardBinding implements Bindings {
  @override
  void dependencies() {
    Get.put(DashboardController(
        accountRepository: Get.find<AccountRepository>(),
        moreTymeRepository: Get.find<MoreTymeRepository>(),
        appCache: Get.find<AppCache>()));
  }
}
