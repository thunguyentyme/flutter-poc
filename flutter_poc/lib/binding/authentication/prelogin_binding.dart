import 'package:flutter_poc/controller/authentication/prelogin.dart';
import 'package:flutter_poc/data/cache/app_cache.dart';
import 'package:flutter_poc/data/repository/authentication.dart';
import 'package:get/get.dart';

class PreLoginBinding implements Bindings {
  @override
  void dependencies() {
    Get.put(PreLoginController(
        repository: Get.find<AuthenticationRepository>(),
        appCache: Get.find<AppCache>()));
  }
}
