import 'package:flutter_poc/controller/authentication/pin.dart';
import 'package:flutter_poc/data/cache/app_cache.dart';
import 'package:flutter_poc/data/repository/authentication.dart';
import 'package:get/get.dart';

class PinBinding implements Bindings {
  @override
  void dependencies() {
    Get.put(PinController(
        repository: Get.find<AuthenticationRepository>(),
        appCache: Get.find<AppCache>()));
  }
}
