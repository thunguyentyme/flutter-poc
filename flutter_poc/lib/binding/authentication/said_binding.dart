import 'package:flutter_poc/controller/authentication/said.dart';
import 'package:flutter_poc/data/cache/app_cache.dart';
import 'package:flutter_poc/data/repository/authentication.dart';
import 'package:get/get.dart';

class SaidBinding implements Bindings {
  @override
  void dependencies() {
    Get.put(SaidController(
        repository: Get.find<AuthenticationRepository>(),
        appCache: Get.find<AppCache>()));
  }
}
