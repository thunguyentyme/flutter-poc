import 'package:flutter_poc/controller/authentication/otp.dart';
import 'package:flutter_poc/data/cache/app_cache.dart';
import 'package:flutter_poc/data/repository/authentication.dart';
import 'package:get/get.dart';

class OtpBinding implements Bindings {
  @override
  void dependencies() {
    Get.put(OtpController(
        repository: Get.find<AuthenticationRepository>(),
        appCache: Get.find<AppCache>()));
  }
}
