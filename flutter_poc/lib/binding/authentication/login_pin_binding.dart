import 'package:flutter_poc/controller/authentication/login_pin.dart';
import 'package:flutter_poc/data/cache/app_cache.dart';
import 'package:flutter_poc/data/repository/authentication.dart';
import 'package:get/get.dart';

class LoginPinBinding implements Bindings {
  @override
  void dependencies() {
    Get.put(LoginPinController(
        repository: Get.find<AuthenticationRepository>(),
        appCache: Get.find<AppCache>()));
  }
}
