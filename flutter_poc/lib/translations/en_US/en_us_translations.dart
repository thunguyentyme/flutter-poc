
final Map<String, String> enUs = {
  //prelogin_page
  'str_already_a_customer' : 'Already a customer?',
  'str_securely_link' : 'Securely link your device to protect your\naccounts from unauthorised access.',
  'str_login' : 'LOGIN',
  'str_not_a_customer_yet' : 'Not a customer yet?',
  'str_open_account_now' : 'OPEN ACCOUNT NOW',

  //said_page
  'str_enter_your_south_said' : 'Enter your South African ID number',
  'str_your_id_number_allow' : 'Your ID number allows us to identify you.',
  'str_id_number' : 'ID number',

  //pin page
  'str_enter_your_login_pin' : 'Enter your login PIN',
  'str_enter_your_login_pin_to_continue' : 'Enter your login PIN to continue',

  //otp page
  'str_enter_your_otp' : 'Enter your OTP',
  'str_enter_the_one_time_pin' : 'Enter the One-Time-Pin (OTP) we have sent as an SMS to ',

  //dash board
};
