import 'package:flutter/material.dart';
import 'package:flutter_poc/utils/colors.dart';
final ThemeData appThemeData = ThemeData(
  primaryColor: AppColor.yellow,
  accentColor: AppColor.yellow,
  splashColor: AppColor.yellow,
  highlightColor: AppColor.yellow,
  // fontFamily: 'Georgia',
  textTheme: TextTheme(
    headline1: TextStyle(fontSize: 72.0, fontWeight: FontWeight.bold),
  ),
);
