import 'package:flutter/material.dart';
import 'package:flutter_poc/utils/colors.dart';

final TextStyle cardTextStyle = TextStyle(
  color: AppColor.grey_30,
  fontSize: 16,
  fontWeight: FontWeight.bold
);