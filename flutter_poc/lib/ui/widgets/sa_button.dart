import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_poc/utils/colors.dart';

class SAButton extends StatelessWidget {
  final String title;
  final Function() action;
  final Color color;
  final double width;
  final double height;
  const SAButton({Key key, this.title, this.action, this.color, this.width, this.height}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return Container(
        height: height != null ? height : 35.0,
        width: width != null ? width : 165,
        decoration: BoxDecoration(
            color: color != null ? color : AppColor.yellow,
            borderRadius: BorderRadius.circular(2)),
        child:  Material(
          color: Colors.transparent,
          child: InkWell(
              splashColor: Colors.grey, // inkwell color
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    title,
                    style: TextStyle(
                      fontWeight: FontWeight.normal,
                      color: Colors.black,
                      fontSize: 15.0,
                      decoration: TextDecoration.none,),
                  )
                ],
              ),
              onTap: () {
                action();
              }),
        )
    );
  }
}

