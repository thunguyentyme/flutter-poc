import 'package:flutter/material.dart';
import 'package:flutter_poc/utils/colors.dart';
import 'package:get/get.dart';

class DashboardToolbarWidget extends StatelessWidget {
  DashboardToolbarWidget({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: Get.width,
      child: Stack(
        children: [
          GestureDetector(
              onTap: () {
                Get.back();
              },
              child: Icon(Icons.keyboard_backspace, color: AppColor.white,)
          ),

          Center(
            child: Image(
              image: AssetImage('assets/images/logo.png'),
              width: 30,
              height: 30,
            ),
          )
        ],
      ),
    );
  }
}

