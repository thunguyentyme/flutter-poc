import 'package:flutter/material.dart';
import 'package:flutter_poc/controller/dashboard/dashboard.dart';
import 'package:flutter_poc/utils/colors.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:percent_indicator/percent_indicator.dart';

class DashboardPage extends GetView<DashboardController> {
  int _selectedIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: AppColor.main_background,
        appBar: AppBar(
          backgroundColor: AppColor.main_background,
          title: const Text('ACCOUNTS'),
          bottom: PreferredSize(
              child: Container(
                color: AppColor.yellow,
                height: 1.0,
              ),
              preferredSize: Size.fromHeight(1.0)),
          leading: Builder(
            builder: (context) => IconButton(
              icon: ImageIcon(
                AssetImage("assets/images/icon_leftmenu.png"),
                color: AppColor.yellow,
              ),
              onPressed: () => Scaffold.of(context).openDrawer(),
            ),
          ),
        ),
        body: Center(
            // child: _widgetOptions.elementAt(_selectedIndex),
            child: _buildHomePage()),
        bottomNavigationBar: _buildBottomNav(),
        drawer: _buildLeftMenu(context));
  }

  void _onItemTapped(int index) {
    _selectedIndex = index;
    print("select item: $index");
  }

  BottomNavigationBar _buildBottomNav() {
    return BottomNavigationBar(
      backgroundColor: AppColor.main_background,
      type: BottomNavigationBarType.fixed,
      selectedLabelStyle: TextStyle(
          color: AppColor.yellow, fontSize: 9.6, fontWeight: FontWeight.bold),
      unselectedLabelStyle: TextStyle(
          color: AppColor.white, fontSize: 9.6, fontWeight: FontWeight.normal),
      unselectedItemColor: AppColor.white,
      unselectedIconTheme: IconThemeData(
        color: AppColor.yellow_disabled,
        opacity: 0.5,
      ),
      selectedIconTheme: IconThemeData(
        color: AppColor.yellow,
        opacity: 1.0,
      ),
      items: const <BottomNavigationBarItem>[
        BottomNavigationBarItem(
          icon: Padding(
            padding: EdgeInsets.all(5.0),
            child: ImageIcon(
              AssetImage("assets/images/icon_pay.png"),
              color: AppColor.yellow,
            ),
          ),
          label: 'Pay',
        ),
        BottomNavigationBarItem(
          icon: Padding(
            padding: EdgeInsets.all(5.0),
            child: ImageIcon(
              AssetImage("assets/images/icon_save.png"),
              color: AppColor.yellow,
            ),
          ),
          label: 'Save',
        ),
        BottomNavigationBarItem(
          icon: Padding(
            padding: EdgeInsets.all(5.0),
            child: ImageIcon(
              AssetImage("assets/images/icon_buy.png"),
              color: AppColor.yellow,
            ),
          ),
          label: 'Buy',
        ),
        BottomNavigationBarItem(
          icon: Padding(
            padding: EdgeInsets.all(5.0),
            child: ImageIcon(
              AssetImage("assets/images/icon_cash.png"),
              color: AppColor.yellow,
            ),
          ),
          label: 'Cash',
        ),
      ],
      // currentIndex: _selectedIndex,
      selectedItemColor: AppColor.white,
      onTap: _onItemTapped,
    );
  }

  Drawer _buildLeftMenu(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: [
          const DrawerHeader(
            decoration: BoxDecoration(
              color: AppColor.main_background,
            ),
            child: Text('Drawer Header'),
          ),
          ListTile(
            title: const Text('Logout'),
            onTap: () {
              Navigator.pop(context);
              controller.logOut();
            },
          ),
          ListTile(
            title: const Text('De-link device'),
            onTap: () {
              Navigator.pop(context);
              controller.delinkDevice();
            },
          ),
        ],
      ),
    );
  }

  Widget _buildHomePage() {
    return SizedBox(
      // height: 210,
      child: Card(
        color: Colors.transparent,
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 5),
            ),
            _buildEverydayAccount(),
            Padding(
              padding: const EdgeInsets.only(top: 5),
            ),
            _buildGoalSaves(),
            Padding(
              padding: const EdgeInsets.only(top: 15),
            ),
            _buildMoreTymeCard(),
          ],
        ),
      ),
    );
  }

  Widget _buildEverydayAccount() {
    return Container(
        color: AppColor.transparent,
        padding: const EdgeInsets.fromLTRB(5, 0, 5, 0),
        height: 70.0,
        child: Container(
          decoration: new BoxDecoration(
              color: AppColor.white,
              borderRadius: new BorderRadius.all(Radius.circular(5.0))),
          padding: const EdgeInsets.fromLTRB(0, 10, 0, 0),
          child: Row(
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 15),
              ),
              Image(
                image: AssetImage('assets/images/everyday_account.png'),
                width: 33,
                height: 33,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 15),
              ),
              Expanded(
                /*1*/
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    /*2*/
                    Container(
                      padding: const EdgeInsets.only(bottom: 8),
                      child: Obx(() => Text(
                        controller.oTransactionAccountName.value,
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                        ),
                      )),
                    ),
                    Obx(() => Text(
                      controller.oTransactionAccountNumber.value,
                      style: TextStyle(
                        color: Colors.grey[500],
                      ),
                    )),
                  ],
                ),
              ),
              /*3*/
              Expanded(
                  /*1*/
                  child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Obx(() => Text(
                    'R ' + controller.oTransactionAccountBalance.value,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  )),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 20),
                  ),
                ],
              )),
              Padding(
                padding: const EdgeInsets.only(right: 15),
              ),
              Image(
                image: AssetImage('assets/images/icon_detail_arrow.png'),
                width: 7.4,
                height: 12,
              ),
              Padding(
                padding: const EdgeInsets.only(right: 15),
              ),
            ],
          ),
        ));
  }

  Widget _buildGoalSaves() {
    return Container(
      color: AppColor.transparent,
      padding: const EdgeInsets.fromLTRB(5, 0, 5, 0),
      height: 70.0,
      child: Container(
          decoration: new BoxDecoration(
              color: AppColor.white,
              borderRadius: new BorderRadius.all(Radius.circular(5.0))),
          padding: const EdgeInsets.fromLTRB(0, 10, 0, 0),
          child: Row(
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 15),
              ),
              Image(
                image: AssetImage('assets/images/goal_save.png'),
                width: 33,
                height: 33,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 15),
              ),
              Expanded(
                /*1*/
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    /*2*/
                    Container(
                      padding: const EdgeInsets.only(bottom: 8),
                      child: Obx(() => Text(
                        controller.oCountGoalSave.value + ' GoalSave(s)',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                        ),
                      )),
                    ),
                  ],
                ),
              ),
              /*3*/
              Expanded(
                  /*1*/
                  child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Obx(() => Text(
                    'R ' + controller.oSumGoalSave.value,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  )),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 20),
                  ),
                ],
              )),
              Padding(
                padding: const EdgeInsets.only(right: 15),
              ),
              Image(
                image: AssetImage('assets/images/icon_detail_arrow.png'),
                width: 7.4,
                height: 12,
              ),
              Padding(
                padding: const EdgeInsets.only(right: 15),
              ),
            ],
          )),
    );
  }

  _buildMoreTymeCard() {
    return Container(
      color: AppColor.transparent,
      padding: const EdgeInsets.fromLTRB(5, 0, 5, 0),
      height: 200.0,
      child: Stack(
        children: [
          Container(
            decoration: new BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight,
                  colors: [
                    AppColor.gradient_top,
                    AppColor.gradient_bottom.withOpacity(0.0),
                  ],
                ),
                border: Border(
                  top: BorderSide(width: 1.0, color: Color(0xFFE3E3E3)),
                  left: BorderSide(width: 1.0, color: Color(0xFFE3E3E3)),
                  right: BorderSide(width: 1.0, color: Color(0xFFE3E3E3)),
                  bottom: BorderSide(width: 1.0, color: Color(0xFFE3E3E3)),
                ),
                borderRadius: new BorderRadius.all(Radius.circular(4.0))),
            child: Stack(
              children: [
                Positioned(
                  bottom: 0,
                  right: 0,
                  child: Container(
                    // We use this Container to create a black box that wraps the white text so that the user can read the text even when the image is white

                    child: Image(
                      image: AssetImage('assets/images/MT_card_bg.png'),
                      width: 148,
                      height: 166,
                    ),
                  ),
                ),
                Container(
                  //padding: const EdgeInsets.all(17),
                  padding: const EdgeInsets.fromLTRB(16, 16, 16, 16),
                  child: Column(
                    children: [
                      Container(
                        color: AppColor.transparent,
                        child: Row(
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                SvgPicture.asset(
                                    'assets/images/icon_MT_logo.svg'),
                                Row(
                                  children: [
                                    const Text(
                                      "View more",
                                      style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        color: AppColor.yellow,
                                      ),
                                    ),
                                    Padding(
                                        padding:
                                            const EdgeInsets.only(left: 4)),
                                    Image(
                                      image: AssetImage(
                                          'assets/images/more_arrow.png'),
                                      width: 7.4,
                                      height: 12,
                                    )
                                  ],
                                ),
                              ],
                            ),
                            Expanded(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: [
                                  Container(
                                    child: Image(
                                      image: AssetImage(
                                          'assets/images/icon_qr_code.png'),
                                      width: 100,
                                      height: 100,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                      Expanded(
                        child: new LinearPercentIndicator(
                          lineHeight: 8.0,
                          percent: 0.2,
                          progressColor: AppColor.yellow,
                          backgroundColor: AppColor.black,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 15),
                        child: Column(
                          children: [
                            Row(
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(top: 15),
                                ),
                                Container(
                                  padding: const EdgeInsets.only(bottom: 8),
                                  child: const Text(
                                    'Available to spend',
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      color: AppColor.white,
                                    ),
                                  ),
                                ),
                                Expanded(
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.end,
                                    children: [
                                      Container(
                                        child: Obx(() => Text(
                                          'R ' + controller.oMTAts.value,
                                          style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            color: AppColor.white,
                                          ),
                                        )),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                            Row(
                              children: [
                                Row(
                                  children: [
                                    const Text(
                                      "MoreTyme limit",
                                      style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        color: AppColor.white,
                                      ),
                                    ),
                                    Padding(
                                        padding:
                                            const EdgeInsets.only(left: 4)),
                                    Image(
                                      image: AssetImage(
                                          'assets/images/icon_info.png'),
                                      width: 18,
                                      height: 18,
                                    )
                                  ],
                                ),
                                Expanded(
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.end,
                                    children: [
                                      Container(
                                        child: Obx(() => Text(
                                          'R ' + controller.oMTLimit.value,
                                          style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            color: AppColor.white,
                                          ),
                                        )),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
