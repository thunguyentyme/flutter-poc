import 'package:flutter/material.dart';
import 'package:flutter_poc/controller/authentication/said.dart';
import 'package:flutter_poc/ui/widgets/toolbar_widget.dart';
import 'package:flutter_poc/utils/colors.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

class SaidPage extends GetView<SaidController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
            padding: const EdgeInsets.all(16.0),
            color: AppColor.main_background,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                ToolbarWidget(),
                Expanded(
                    child: ListView(
                  shrinkWrap: true,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(top: 30, bottom: 12),
                      child: Text(
                        'str_enter_your_south_said'.tr,
                        style: TextStyle(
                            color: AppColor.white,
                            fontSize: 20,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                    Text(
                      'str_your_id_number_allow'.tr,
                      style: TextStyle(color: AppColor.white),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 16),
                      child: Text(
                        'str_id_number'.tr,
                        style: TextStyle(color: AppColor.yellow),
                      ),
                    ),
                    TextField(
                      controller: controller.saidController,
                      keyboardType: TextInputType.number,
                      style: TextStyle(color: AppColor.white),
                      maxLength: 13,
                      decoration: InputDecoration(
                        hintText: '812345 1234 12 3',
                        hintStyle: TextStyle(color: AppColor.grey_30),
                        enabledBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: AppColor.grey_30),
                        ),
                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: AppColor.yellow),
                        ),
                      ),
                    ),
                  ],
                )),
                Align(
                    alignment: Alignment.bottomCenter,
                    child: GestureDetector(
                        onTap: () {
                          controller.goToPinPage();
                        },
                        child: SvgPicture.asset(
                            'assets/images/circle_btn_enable.svg')))
              ],
            )),
      ),
    );
  }
}
