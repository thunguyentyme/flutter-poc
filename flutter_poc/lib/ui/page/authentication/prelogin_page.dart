import 'package:flutter/material.dart';
import 'package:flutter_poc/controller/authentication/prelogin.dart';
import 'package:flutter_poc/ui/widgets/sa_button.dart';
import 'package:flutter_poc/utils/colors.dart';
import 'package:get/get.dart';

class PreLoginPage extends GetView<PreLoginController> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          width: Get.width,
          height: Get.height,
          color: AppColor.main_background,
          child: Column(
            children: [
              SizedBox(height: 10,),

              Image(
                image: AssetImage('assets/images/logo.png'),
                width: 30,
                height: 30,
              ),

              SizedBox(height: 10,),

              Image(
                  image: AssetImage('assets/images/logo_text.png')
              ),

              SizedBox(height: 30,),

              Text(
                'str_already_a_customer'.tr,
                style: TextStyle(color: AppColor.yellow),
              ),

              Padding(
                padding: const EdgeInsets.fromLTRB(0, 16, 0, 16),
                child: Text(
                  'str_securely_link'.tr,
                  textAlign: TextAlign.center,
                  style: TextStyle(color: AppColor.white, ),
                ),
              ),

              SAButton(
                title: 'str_login'.tr,
                action: () {
                  controller.onClickLogin();
                },
              ),

              Padding(
                padding: const EdgeInsets.fromLTRB(0, 30, 0, 16),
                child: Text(
                  'str_not_a_customer_yet'.tr,
                  style: TextStyle(color: AppColor.yellow),
                ),
              ),

              Container(
                  width: 165,
                  height: 35,
                  decoration: BoxDecoration(
                      border: Border.all(color: AppColor.grey_30)),
                  child: Center(
                    child: Text(
                      'str_open_account_now'.tr,
                      style: TextStyle(
                          color: Colors.white, fontWeight: FontWeight.bold),
                    ),
                  )
              ),
            ],
          ),
        ),
      ),
    );
  }
}