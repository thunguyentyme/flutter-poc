import 'dart:async';

import 'package:flutter_poc/controller/authentication/otp.dart';
import 'package:flutter_poc/ui/widgets/toolbar_widget.dart';
import 'package:flutter_poc/utils/colors.dart';
import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:pin_code_fields/pin_code_fields.dart';

class OtpPage extends GetView<OtpController> {
  final StreamController<ErrorAnimationType> errorController = StreamController<ErrorAnimationType>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
            padding: const EdgeInsets.all(16.0),
            color: AppColor.main_background,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                ToolbarWidget(),

                Padding(
                  padding: const EdgeInsets.only(top: 30, bottom: 12),
                  child: Text(
                    'str_enter_your_otp'.tr,
                    style: TextStyle(color: AppColor.white, fontSize: 20, fontWeight: FontWeight.bold),
                  ),
                ),

                Obx(() => Text(
                  'str_enter_the_one_time_pin'.tr + controller.oDeliveryTo.value,
                  style: TextStyle(color: AppColor.white),
                )),

                Form(
                  child: Padding(
                    padding: const EdgeInsets.only(left: 80, right: 80, top: 20),
                    child: PinCodeTextField(
                      length: 4,
                      obscureText: true,
                      animationType: AnimationType.fade,
                      textStyle: TextStyle(color: Colors.white),
                      pinTheme: PinTheme(
                        shape: PinCodeFieldShape.box,
                        // borderRadius: BorderRadius.circular(5),
                        fieldHeight: 40,
                        fieldWidth: 40,
                        activeFillColor: AppColor.main_background,
                        activeColor: AppColor.white,
                        inactiveColor: AppColor.yellow,
                        inactiveFillColor: AppColor.main_background,
                        selectedFillColor: AppColor.main_background,
                        selectedColor: AppColor.yellow,
                      ),
                      animationDuration: Duration(milliseconds: 300),
                      enableActiveFill: true,
                      errorAnimationController: errorController,
                      controller: controller.otpController,
                      autoDisposeControllers: false,
                      keyboardType: TextInputType.number,
                      onCompleted: (v) {
                        controller.onOtpEntered();
                      },
                      onChanged: (value) {
                        print(value);
                        // setState(() {
                        //   currentText = value;
                        // });
                      },
                      beforeTextPaste: (text) {
                        print("Allowing to paste $text");
                        return true;
                      }, appContext: context,
                    ),
                  ),
                )
              ],
            )
        ),
      ),
    );
  }
}