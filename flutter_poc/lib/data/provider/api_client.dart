import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_poc/config/AppConfig.dart';
import 'package:flutter_poc/data/cache/app_cache.dart';
import 'package:flutter_poc/data/model/account.dart';
import 'package:flutter_poc/data/model/authentication_info.dart';
import 'package:flutter_poc/data/model/moretyme_account.dart';
import 'package:flutter_poc/data/model/otp_info.dart';
import 'package:flutter_poc/data/model/token_info.dart';
import 'package:flutter_poc/data/provider/field_list.dart';
import 'package:flutter_poc/data/provider/header_list.dart';
import 'package:flutter_poc/utils/utils.dart';
import 'package:meta/meta.dart';

import 'api_list.dart';

class HttpApiClient {
  final Dio httpClient;
  final AppCache appCache;

  HttpApiClient(
      {@required this.httpClient,
      @required this.appCache});

  Future<AuthenticationInfo> login(String said, String pin, String clientId) async {
    var headers = HeaderList.getDefaultHeader();
    headers.addAll({
      HeaderList.authorization: "Basic ${Utils.encryptLoginPin(said, pin)}"
    });

    var data = {
      FieldList.redirectUri: "http://localhost/cbsa/callback",
      FieldList.responseType: "code",
      FieldList.clientId: clientId,
      FieldList.scope: "openid device_" + Utils.getSerialNumber(),
      FieldList.loginMethod: "PIN"
    };

    var response = await httpClient.get(ApiList.login,
        options: Options(
            headers: headers,
            validateStatus: (code) {
              return code == 302;
            }),
        queryParameters: data);

    return compute(mapAuthenticationInfo, response.headers.map["location"]);
  }

  Future<TokenInfo> generateToken(String code, String clientId, String clientSecret) async {
    var headers = HeaderList.getDefaultHeader();
    headers.addAll(
        {HeaderList.contentType: HeaderList.formUrlEncodeV2ContentType});

    var data = {
      FieldList.clientId: clientId,
      FieldList.clientSecret: clientSecret,
      FieldList.grantType: "authorization_code",
      FieldList.redirectUri: "http://localhost/cbsa/callback",
      FieldList.code: code
    };

    var response = await httpClient.post(ApiList.token,
        options: Options(headers: headers), data: data);

    return compute(mapTokenInfo, Map.from(response.data));
  }

  Future<String> generateOtpRequestToken() async {
    var headers = HeaderList.getDefaultHeader();
    headers.addAll({
      HeaderList.authorization: appCache.authorizationToken,
      HeaderList.deviceId: Utils.getDeviceId(),
      HeaderList.deviceName: Utils.getDeviceName(),
      HeaderList.deviceOS: await Utils.getDeviceOS(),
      HeaderList.accept: Headers.jsonContentType,
      HeaderList.stepUpToken: ""
    });

    var response = await httpClient.post(ApiList.addDevice,
        options: Options(
            headers: headers,
            validateStatus: (statusCode) {
              return statusCode == 403;
            }));

    if (response.data["errorCode"] != "0407010") {
      throw Exception("Can not get request token for OTP");
    }

    return response.headers.map[HeaderList.requestToken][0];
  }

  Future<OtpInfo> requestOtp(String requestToken) async {
    var headers = HeaderList.getDefaultHeader();
    headers.addAll({
      HeaderList.authorization: appCache.authorizationToken,
      HeaderList.deliveryMethod: "SMS"
    });

    var data = {FieldList.requestToken: requestToken};

    var response = await httpClient.post(ApiList.otp,
        options: Options(headers: headers), data: data);

    return compute(mapOtpInfo, Map.from(response.data));
  }

  Future<String> verifyOtp(String requestToken, String otpCode) async {
    var headers = HeaderList.getDefaultHeader();
    headers.addAll({
      HeaderList.authorization: appCache.authorizationToken,
      HeaderList.deliveryMethod: "SMS",
      HeaderList.accept: Headers.jsonContentType
    });

    var data = {
      FieldList.otpCode: otpCode,
      FieldList.requestToken: requestToken
    };

    var response = await httpClient.post(ApiList.otpVerification,
        options: Options(headers: headers), data: data);

    return response.data["stepUpToken"];
  }

  Future<bool> addDevice(String stepUpToken) async {
    var headers = HeaderList.getDefaultHeader();
    headers.addAll({
      HeaderList.authorization: appCache.authorizationToken,
      HeaderList.deviceId: Utils.getDeviceId(),
      HeaderList.deviceName: Utils.getDeviceName(),
      HeaderList.deviceOS: await Utils.getDeviceOS(),
      HeaderList.accept: Headers.jsonContentType,
      HeaderList.stepUpToken: stepUpToken
    });

    await httpClient.post(ApiList.addDevice,
        options: Options(headers: headers));

    return true;
  }

  Future<List<Account>> getAccounts() async {
    var headers = HeaderList.getDefaultHeader();
    headers.addAll({ HeaderList.authorization: appCache.authorizationToken });
    headers.remove(HeaderList.deviceIdV2);

    var response = await httpClient.get(ApiList.account,
        options: Options(headers: headers));

    return compute(mapAccounts, response.data);
  }

  Future<MoreTymeAccount> getMoreTymeAccount() async {
    var headers = HeaderList.getDefaultHeader();
    headers.addAll({HeaderList.authorization: appCache.authorizationToken});

    var response = await httpClient.get(ApiList.moretymeAccount,
        options: Options(headers: headers));

    return compute(mapMoreTymeAccount, Map.from(response.data));
  }
}
