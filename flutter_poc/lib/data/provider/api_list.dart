class ApiList {
  static const login = "/authorize";
  static const token = "/token";
  static const otp = "/otp/v1.0.0/otp/";
  static const otpVerification = "/otp/v1.0.0/otp/verification/request-token";
  static const addDevice = "/ds/v1.0.0/device/trusted-device";
  static const account = "/as/v1.0.0/account/";
  static const moretymeAccount = "/moretyme/v1.0.0/account";
}