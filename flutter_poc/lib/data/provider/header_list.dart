import 'package:dio/dio.dart';
import 'package:flutter_poc/utils/utils.dart';
import 'package:uuid/uuid.dart';
import 'package:intl/intl.dart';

class HeaderList {
  static const correlationId = "Correlation-Id";
  static const timestamp = "Timestamp";
  static const callerId = "Caller-Id";
  static const channel = "Channel";
  static const sessionId = "Session-Id";
  static const authorization = "Authorization";
  static const deviceInfo = "Device-Info";
  static const contentType = "Content-Type";
  static const deliveryMethod = "Delivery-Method";
  static const deviceOS = "Device-OS";
  static const deviceId = "Device-Id";
  static const deviceIdV2 = "Device_Id";
  static const deviceName = "Device-Name";
  static const accept = "Accept";
  static const stepUpToken = "Step-Up-Token";
  static const requestToken = "request-token";

  static const formUrlEncodeV2ContentType = "application/x-www-form-urlencoded;charset=UTF-8";

  static Map<String, String> getDefaultHeader() {
    return {
      correlationId: Uuid().v4(),
      timestamp: DateFormat("yyyy-MM-dd").format(DateTime.now()),
      callerId: "Channel.SmartApp",
      channel: "Channel.SmartApp",
      contentType: Headers.jsonContentType,
      sessionId: "1d948834-a1de-11e7-abc4-cec278b6b50a",
      deviceInfo: Utils.getDeviceInfoHeader(),
      deviceIdV2: Utils.getDeviceId()
    };
  }
}