class FieldList {
  static const redirectUri = "redirect_uri";
  static const responseType = "response_type";
  static const clientId = "client_id";
  static const clientSecret = "client_secret";
  static const scope = "scope";
  static const loginMethod = "loginMethod";
  static const grantType = "grant_type";
  static const code = "code";
  static const requestToken = "requestToken";
  static const otpCode = "otpCode";
}