import 'package:flutter_poc/data/model/account.dart';
import 'package:flutter_poc/data/provider/api_client.dart';
import 'package:meta/meta.dart';

class AccountRepository {
  final HttpApiClient apiClient;
  AccountRepository({@required this.apiClient}) : assert(apiClient != null);

  Future<List<Account>> getAccounts() {
    return apiClient.getAccounts();
  }

}