import 'package:flutter_poc/data/model/moretyme_account.dart';
import 'package:flutter_poc/data/provider/api_client.dart';
import 'package:meta/meta.dart';

class MoreTymeRepository {
  final HttpApiClient apiClient;
  MoreTymeRepository({@required this.apiClient}) : assert(apiClient != null);

  Future<MoreTymeAccount> getAccount() {
    return apiClient.getMoreTymeAccount();
  }

}