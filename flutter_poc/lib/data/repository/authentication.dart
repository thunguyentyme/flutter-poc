import 'package:flutter_poc/config/AppConfig.dart';
import 'package:flutter_poc/data/model/authentication_info.dart';
import 'package:flutter_poc/data/model/otp_info.dart';
import 'package:flutter_poc/data/model/token_info.dart';
import 'package:flutter_poc/data/provider/api_client.dart';
import 'package:meta/meta.dart';

class AuthenticationRepository {
  final HttpApiClient apiClient;
  final AppConfig appConfig;

  AuthenticationRepository({@required this.apiClient, @required this.appConfig})
      : assert(apiClient != null),
        assert(appConfig != null);

  Future<AuthenticationInfo> login(String said, String pin) {
    return apiClient.login(said, pin, appConfig.getClientIdNonDevice());
  }

  Future<TokenInfo> generateToken(String code) {
    return apiClient.generateToken(code, appConfig.getClientIdNonDevice(),
        appConfig.getClientSecretNonDevice());
  }

  Future<AuthenticationInfo> loginOnLinkedDevice(String said, String pin) {
    return apiClient.login(said, pin, appConfig.getClientIdLinkedDevice());
  }

  Future<TokenInfo> generateTokenOnLinkedDevice(String code) {
    return apiClient.generateToken(code, appConfig.getClientIdLinkedDevice(),
        appConfig.getClientSecretLinkedDevice());
  }

  Future<String> generateOtpRequestToken() {
    return apiClient.generateOtpRequestToken();
  }

  Future<OtpInfo> requestOtp(String requestToken) {
    return apiClient.requestOtp(requestToken);
  }

  Future<String> verifyOtp(String requestToken, String otpCode) {
    return apiClient.verifyOtp(requestToken, otpCode);
  }

  Future<bool> addDevice(String stepUpToken) {
    return apiClient.addDevice(stepUpToken);
  }
}
