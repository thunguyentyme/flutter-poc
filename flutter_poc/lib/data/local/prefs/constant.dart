class Constant {
  static const saidKey = "SAID_KEY";
  static const pinKey = "PIN_KEY";
  static const accessToken = "ACCESS_TOKEN_KEY";
}