class Account {
  String accountNumber;
  String accountName;
  String accountStatus;
  String accountTypeCode;
  String balance;
  String accruedInterest;

  bool isActive() {
    return accountStatus.compareTo("Active") == 0;
  }

  bool isTransactionAccount() {
    return accountTypeCode.compareTo("CURRENT_ACCOUNT") == 0;
  }

  bool isGoalSaveAccount() {
    return accountTypeCode.compareTo("REGULAR_SAVINGS") == 0;
  }
}

Future<List<Account>> mapAccounts(dynamic listData) async {
  return await Stream.fromIterable(listData)
  .map((data) {
    Account info = new Account();
    info.accountNumber = data["accountNumber"];
    info.accountName = data["accountName"];
    info.accountStatus = data["statuses"][0]["accountStatus"] ?? "";
    info.accountTypeCode = data["accountTypeCode"];
    info.balance = data["balance"];
    info.accruedInterest = data["accruedInterest"];
    return info;
  }).toList();
}
