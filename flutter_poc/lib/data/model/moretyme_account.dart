
class MoreTymeAccount {
  String accountNumber;
  String accountStatus;
  String accountId;
  String ats;
  String limit;
}

MoreTymeAccount mapMoreTymeAccount(Map data) {
  MoreTymeAccount info = new MoreTymeAccount();
  info.accountNumber = data["accountNumber"];
  info.accountStatus = data["accountStatus"];
  info.accountId = data["accountId"];
  info.ats = data["availableToSpend"];
  info.limit = data["moreTymeLimit"];

  return info;
}