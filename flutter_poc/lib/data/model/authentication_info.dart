import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';

class AuthenticationInfo {
  String code;
}

AuthenticationInfo mapAuthenticationInfo(List<String> locationMaps) {
  AuthenticationInfo info = AuthenticationInfo();

  if (locationMaps != null) {
    String location = locationMaps[0];
    String code = "";
    if (location != null) {
      try {
        code = Uri.parse(location).queryParameters["code"];
      } catch (e) {}
    }

    info.code = code;
  }

  return info;
}