import 'package:flutter/foundation.dart';

class OtpInfo {
  String deliveryTo;
}

OtpInfo mapOtpInfo(Map data) {
  var info = OtpInfo();
  info.deliveryTo = data["deliverTo"];

  return info;
}