import 'package:flutter/foundation.dart';

class TokenInfo {
  String accessToken;
  String refreshToken;
  String tokenType;
}

TokenInfo mapTokenInfo(Map data) {
  var token = TokenInfo();
  token.accessToken = data["access_token"];
  token.refreshToken = data["refresh_token"];
  token.tokenType = data["token_type"];

  return token;
}
