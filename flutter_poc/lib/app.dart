import 'package:flutter/material.dart';
import 'package:flutter_poc/route/app_pages.dart';
import 'package:flutter_poc/translations/app_translations.dart';
import 'package:flutter_poc/utils/utils.dart';
import 'package:get/get.dart';
import 'flavors.dart';
import 'ui/theme/app_theme.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Utils.generateDeviceInfoHeader();
    return GetMaterialApp(
      title: F.title,
      debugShowCheckedModeBanner: false,
      initialRoute: Routes.INITIAL,
      theme: appThemeData,
      defaultTransition: Transition.fade,
      locale: Locale('en', 'US'),
      translationsKeys: AppTranslation.translations,
      getPages: AppPages.pages,
    );
  }

}