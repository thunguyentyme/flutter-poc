import 'package:flutter_poc/binding/authentication/login_pin_binding.dart';
import 'package:flutter_poc/binding/authentication/otp_binding.dart';
import 'package:flutter_poc/binding/authentication/pin_binding.dart';
import 'package:flutter_poc/binding/authentication/prelogin_binding.dart';
import 'package:flutter_poc/binding/authentication/said_binding.dart';
import 'package:flutter_poc/binding/common/common_binding.dart';
import 'package:flutter_poc/binding/dashboard/dashboard_binding.dart';
import 'package:flutter_poc/controller/authentication/login_pin.dart';
import 'package:flutter_poc/controller/authentication/pin.dart';
import 'package:flutter_poc/ui/page/authentication/otp_page.dart';
import 'package:flutter_poc/ui/page/authentication/pin_page.dart';
import 'package:flutter_poc/ui/page/authentication/prelogin_page.dart';
import 'package:flutter_poc/ui/page/authentication/said_page.dart';
import 'package:flutter_poc/ui/page/dashboard/dashboard_page.dart';
import 'package:get/get.dart';

part './app_routes.dart';

class AppPages {
  static final pages = [
    GetPage(
        name: Routes.INITIAL,
        page: () => PreLoginPage(),
        bindings: [CommonBindings(), PreLoginBinding()]),
    GetPage(
        name: Routes.LOGIN_PIN,
        page: () => PinPage<LoginPinController>(),
        bindings: [CommonBindings(), LoginPinBinding()]),
    GetPage(
        name: Routes.SAID,
        page: () => SaidPage(),
        bindings: [CommonBindings(), SaidBinding()]),
    GetPage(
        name: Routes.PIN,
        page: () => PinPage<PinController>(),
        bindings: [CommonBindings(), PinBinding()]),
    GetPage(
        name: Routes.OTP,
        page: () => OtpPage(),
        bindings: [CommonBindings(), OtpBinding()]),
    GetPage(
        name: Routes.DASH_BOARD,
        page: () => DashboardPage(),
        bindings: [CommonBindings(), DashboardBinding()]),
  ];
}
