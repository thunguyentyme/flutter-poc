part of './app_pages.dart';

abstract class Routes {

  static const INITIAL = '/';
  static const PIN = '/pin';
  static const OTP = '/otp';
  static const DASH_BOARD = '/dash_board';
  static const PRE_LOGIN = '/pre_login';
  static const LOGIN_PIN = '/login_pin';
  static const SAID = '/said';
}